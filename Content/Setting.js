﻿function showMyPrompt() {
    return prompt("لطفا آدرس فید خود را به صورت کامل و صحیح وارد نمایید مثلا:\n" +
        "khabaronline.ir/rss/tp/83\n" +
        "\n" +
        "همچنین شما می‌توانید بجای فید، آدرس صفحه اینستاگرام را درج کنید مثلا:\n" +
        "instagram.com/imamhussainorg\n" +
        "(فقط عکس به همراه کپشن منتقل میشود)\n" +
        "همچنین شما می‌توانید بجای صفحه اینستا، یک هشتگ وارد کنید تا تمام مطالب دارای آن هشتگ در اینستا به عنوان ورودی لحاظ شود:\n" +
        "#امام_حسین\n" +
        "\n" +
        "همچنین شما می‌توانید بجای فید، آدرس یک کانال عمومی تلگرام را درج کنید مثلا:\n" +
        "t.me/tg_ir\n" +
        "(از کانال فقط متن و عکس منتقل میشود / بتا)", "http://");
}


function showMyPromptForNewCHannel() {
    return prompt("نام کانال ، گروه خود را وارد نمایید " +
        "\n" +
        "برای مثال " +
        "\n" +
        "t.me/tg_ir\n" +
        "یا" +
        "\n" +
        "@exampleChannelName" +
        "\n" +
        "توجه نمایید که بت مدیر ابتدا باید به کانال شما اضافه شده و ادمین باشد" +
        "\n" +
        "اگر کانال شما خصوصی است، کافی است یک مطلب از آن را به ربات مدیر فوروارد کنید تا در اینجا اضافه شود", "http://");
}

function removeFeed(hash, feedId) {
    var txt = confirm("آیا از حذف این مورد اطمینان دارید؟");
    if (!txt) {
        return;
    }

    /* return Json(new MyJsonResponse<Feed>
                    {
                        Type = MyJsonResponseType.Success,
                        Single = feed
                    });*/
    $.ajax({
        url: '/feeds/Remove',
        method: 'post',
        data: {hash: hash, feedId: feedId},
        success: function (res) {

            var parsed = res;
            if (parsed.Type === 1) {
                $('#f_' + feedId).hide();
            } else {
                alert(parsed.Message)
            }


        }, fail: function () {

        }
    })
}

/*public enum FeedType
{
    WebRss,
        Hashtag,
        Instagram,
        Telegram,
}*/

function getTypeName(type) {
    if (type == 0)
        return "RSS";
    else if (type == 1)
        return "هشتگ";
    else if (type == 2)
        return "اینستاگرام";
    else if (type == 1)
        return "تلگرام";
}

function addNewFeed(hash, channelId) {
    var txt = showMyPrompt();
    if (txt == null || txt == "") {
       // showMyPrompt();

        return;
    }


    /* return Json(new MyJsonResponse<Feed>
                    {
                        Type = MyJsonResponseType.Success,
                        Single = feed
                    });*/
    $.ajax({
        url: '/feeds/Save',
        method: 'post',
        data: {hash: hash, channelId: channelId, address: txt},
        success: function (res) {

            var parsed = res;
            if (parsed.Type === 1) {

                debugger;
                var typeName = getTypeName(parsed.Single.Type);
                var html = " <tr id=\"f_"+parsed.Single.Id+"\">" +
                    "                                    <td>" +
                    "                                      " + typeName + "                " +
                    "                                    </td>" +
                    "                                    <td>" +
                    "                                        <button class=\"btn btn-danger\" onclick=\"removeFeed('" + hash + "', '" + parsed.Single.Id + "')\">" +
                    "                                            x" +
                    "                                        </button>" +
                    "                                    </td>" +
                    "                                    <td>" +
                    "                                        " + parsed.Single.Address + " " +
                    "                                    </td>" +
                    "                                         <td>\n<a class=\"btn btn-info\" \n" +
                    "                                                    href=\"/feeds/detail?id="+parsed.Single.Id+"&hash="+hash+"\">\n" +
                    "                                                تنظیمات\n" +
                    "                                            </a></td>\n " +
                    "            </tr>";
                $('#feeds_of_ch_' + channelId).append(html);

            } else {
                alert(parsed.Message)
            }


        }, fail: function () {

        }
    })
}


function addNewChannel(hash) {
    var txt = showMyPromptForNewCHannel();
    if (txt == null || txt == "") {
        showMyPromptForNewCHannel();

        return;
    }


    /* return Json(new MyJsonResponse<Feed>
                    {
                        Type = MyJsonResponseType.Success,
                        Single = feed
                    });*/
    $.ajax({
        url: '/channel/Save',
        method: 'post',
        data: {hash: hash, address: txt},
        success: function (res) {

            var parsed = res;
            if (parsed.Type === 1) {
                var html = "<tr>" +
                    "                <td>" +
                    "                    <b>" + parsed.Single.ChatTitle + "</b>" +
                    "                    <b>" + parsed.Single.ChatId + "</b>" +
                    "" +
                    "                </td>" +
                    "                <td>" +
                    "                    <a" +
                    "                        onclick=\"removeChannel('" + hash + "','" + parsed.Single.Id + "')\"" +
                    "                        id=\"removeChannel\" href=\"#\">" +
                    "                        حذف" +
                    "                    </a>" +
                    "" +
                    "                </td>" +
                    "                <tr>" +
                    "                    <td>" +
                    "                        <a" +
                    "                            onclick=\"addNewFeed('" + hash + "', '" + parsed.Single.Id + "')\"" +
                    "                            id=\"addFeed\" href=\"#\">" +
                    "                            افزودن فید جدید" +
                    "                        </a>" +
                    "                    </td>" +
                    "" +
                    "                </tr>" +
                    "            </tr>";

                $('#channels').append(html);

            } else {
                alert(parsed.Message)
            }


        }, fail: function () {

        }
    })
}


function removeChannel(hash, channelId) {
    var txt = confirm("آیا از حذف این کانال اطمینان دارید؟");
    if (!txt) {
        return;
    }

    /* return Json(new MyJsonResponse<Feed>
                    {
                        Type = MyJsonResponseType.Success,
                        Single = feed
                    });*/
    $.ajax({
        url: '/channel/Remove',
        method: 'post',
        data: {hash: hash, id: channelId},
        success: function (res) {

            var parsed = res;
            if (parsed.Type === 1) {
                location.reload();
            } else {
                alert(parsed.Message)
            }


        }, fail: function () {

        }
    })
}

/*

$(document).ready(function () {


    $('#addFeed').click(function () {


    });
});*/
